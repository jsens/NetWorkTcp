﻿namespace WinFrom_TcpServer
{
    partial class Server_Form
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.richTextBox_Messge = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listBox_OnlineList = new System.Windows.Forms.ListBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.给选中连接发送测试数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.群发测试数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comboBox_IP = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_Port = new System.Windows.Forms.TextBox();
            this.button_Startlisten = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label_OnlineCount = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(28, 82);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(865, 488);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.richTextBox_Messge);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(659, 482);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "消息";
            // 
            // richTextBox_Messge
            // 
            this.richTextBox_Messge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_Messge.Location = new System.Drawing.Point(3, 17);
            this.richTextBox_Messge.Name = "richTextBox_Messge";
            this.richTextBox_Messge.Size = new System.Drawing.Size(653, 462);
            this.richTextBox_Messge.TabIndex = 0;
            this.richTextBox_Messge.Text = "";
            this.richTextBox_Messge.DoubleClick += new System.EventHandler(this.richTextBox_Messge_DoubleClick);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox2.Controls.Add(this.listBox_OnlineList);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(668, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(194, 482);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "在线连接";
            // 
            // listBox_OnlineList
            // 
            this.listBox_OnlineList.ContextMenuStrip = this.contextMenuStrip1;
            this.listBox_OnlineList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox_OnlineList.FormattingEnabled = true;
            this.listBox_OnlineList.ItemHeight = 12;
            this.listBox_OnlineList.Location = new System.Drawing.Point(3, 17);
            this.listBox_OnlineList.Name = "listBox_OnlineList";
            this.listBox_OnlineList.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBox_OnlineList.Size = new System.Drawing.Size(188, 462);
            this.listBox_OnlineList.TabIndex = 1;
            this.listBox_OnlineList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_OnlineList_MouseDown);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.给选中连接发送测试数据ToolStripMenuItem,
            this.群发测试数据ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(209, 48);
            this.contextMenuStrip1.Text = "发送消息";
            // 
            // 给选中连接发送测试数据ToolStripMenuItem
            // 
            this.给选中连接发送测试数据ToolStripMenuItem.Name = "给选中连接发送测试数据ToolStripMenuItem";
            this.给选中连接发送测试数据ToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.给选中连接发送测试数据ToolStripMenuItem.Text = "给选中连接发送测试数据";
            this.给选中连接发送测试数据ToolStripMenuItem.Click += new System.EventHandler(this.给选中连接发送测试数据ToolStripMenuItem_Click);
            // 
            // 群发测试数据ToolStripMenuItem
            // 
            this.群发测试数据ToolStripMenuItem.Name = "群发测试数据ToolStripMenuItem";
            this.群发测试数据ToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.群发测试数据ToolStripMenuItem.Text = "群发测试数据";
            this.群发测试数据ToolStripMenuItem.Click += new System.EventHandler(this.群发测试数据ToolStripMenuItem_Click);
            // 
            // comboBox_IP
            // 
            this.comboBox_IP.FormattingEnabled = true;
            this.comboBox_IP.Location = new System.Drawing.Point(120, 26);
            this.comboBox_IP.Name = "comboBox_IP";
            this.comboBox_IP.Size = new System.Drawing.Size(121, 20);
            this.comboBox_IP.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 10F);
            this.label1.Location = new System.Drawing.Point(46, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "监听地址:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 10F);
            this.label2.Location = new System.Drawing.Point(296, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 14);
            this.label2.TabIndex = 2;
            this.label2.Text = "端口号:";
            // 
            // textBox_Port
            // 
            this.textBox_Port.Location = new System.Drawing.Point(355, 25);
            this.textBox_Port.Name = "textBox_Port";
            this.textBox_Port.Size = new System.Drawing.Size(100, 21);
            this.textBox_Port.TabIndex = 3;
            this.textBox_Port.Text = "45678";
            this.textBox_Port.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_Port_KeyPress);
            // 
            // button_Startlisten
            // 
            this.button_Startlisten.Font = new System.Drawing.Font("宋体", 10F);
            this.button_Startlisten.Location = new System.Drawing.Point(508, 22);
            this.button_Startlisten.Name = "button_Startlisten";
            this.button_Startlisten.Size = new System.Drawing.Size(118, 26);
            this.button_Startlisten.TabIndex = 4;
            this.button_Startlisten.Text = "创建监听";
            this.button_Startlisten.UseVisualStyleBackColor = true;
            this.button_Startlisten.Click += new System.EventHandler(this.button_Startlisten_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button_Startlisten);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox_Port);
            this.groupBox1.Controls.Add(this.comboBox_IP);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(28, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(685, 64);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "创建监听";
            // 
            // label_OnlineCount
            // 
            this.label_OnlineCount.AutoSize = true;
            this.label_OnlineCount.Font = new System.Drawing.Font("华文楷体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_OnlineCount.Location = new System.Drawing.Point(724, 33);
            this.label_OnlineCount.Name = "label_OnlineCount";
            this.label_OnlineCount.Size = new System.Drawing.Size(137, 23);
            this.label_OnlineCount.TabIndex = 2;
            this.label_OnlineCount.Text = "在线连接数:0";
            // 
            // Server_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(922, 595);
            this.Controls.Add(this.label_OnlineCount);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Server_Form";
            this.Text = "Server";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ComboBox comboBox_IP;
        private System.Windows.Forms.Button button_Startlisten;
        private System.Windows.Forms.TextBox textBox_Port;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox richTextBox_Messge;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox listBox_OnlineList;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label_OnlineCount;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 给选中连接发送测试数据ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 群发测试数据ToolStripMenuItem;
    }
}

