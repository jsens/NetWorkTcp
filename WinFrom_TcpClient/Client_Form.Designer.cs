﻿namespace WinFrom_TcpClient
{
    partial class Client_Form
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Start = new System.Windows.Forms.Button();
            this.textBox_Address = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.richTextBox_Messge = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.richTextBox_SendMessge = new System.Windows.Forms.RichTextBox();
            this.listBox_ConnList = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox_ThreadNum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.button_ReturnSendMessge = new System.Windows.Forms.Button();
            this.button_ReturnSendObject = new System.Windows.Forms.Button();
            this.button_SendMessge = new System.Windows.Forms.Button();
            this.button_SenObject = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_Start
            // 
            this.button_Start.Font = new System.Drawing.Font("宋体", 10F);
            this.button_Start.Location = new System.Drawing.Point(494, 12);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(118, 26);
            this.button_Start.TabIndex = 3;
            this.button_Start.Text = "开始连接";
            this.button_Start.UseVisualStyleBackColor = true;
            this.button_Start.Click += new System.EventHandler(this.button_Startlisten_Click);
            // 
            // textBox_Address
            // 
            this.textBox_Address.Location = new System.Drawing.Point(94, 35);
            this.textBox_Address.Name = "textBox_Address";
            this.textBox_Address.Size = new System.Drawing.Size(136, 21);
            this.textBox_Address.TabIndex = 1;
            this.textBox_Address.Text = "192.168.0.16:45678";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 10F);
            this.label2.Location = new System.Drawing.Point(6, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 14);
            this.label2.TabIndex = 5;
            this.label2.Text = "服务端地址:";
            // 
            // richTextBox_Messge
            // 
            this.richTextBox_Messge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_Messge.Location = new System.Drawing.Point(3, 17);
            this.richTextBox_Messge.Name = "richTextBox_Messge";
            this.richTextBox_Messge.Size = new System.Drawing.Size(643, 505);
            this.richTextBox_Messge.TabIndex = 4;
            this.richTextBox_Messge.Text = "";
            this.richTextBox_Messge.DoubleClick += new System.EventHandler(this.richTextBox_Messge_DoubleClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.richTextBox_Messge);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 83);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(649, 525);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "消息";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.richTextBox_SendMessge);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(209, 513);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "发送消息";
            // 
            // richTextBox_SendMessge
            // 
            this.richTextBox_SendMessge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_SendMessge.Location = new System.Drawing.Point(3, 17);
            this.richTextBox_SendMessge.Name = "richTextBox_SendMessge";
            this.richTextBox_SendMessge.Size = new System.Drawing.Size(203, 493);
            this.richTextBox_SendMessge.TabIndex = 6;
            this.richTextBox_SendMessge.Text = "";
            // 
            // listBox_ConnList
            // 
            this.listBox_ConnList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox_ConnList.FormattingEnabled = true;
            this.listBox_ConnList.Location = new System.Drawing.Point(3, 19);
            this.listBox_ConnList.Margin = new System.Windows.Forms.Padding(5);
            this.listBox_ConnList.Name = "listBox_ConnList";
            this.listBox_ConnList.Size = new System.Drawing.Size(208, 589);
            this.listBox_ConnList.TabIndex = 5;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.listBox_ConnList);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("宋体", 10F);
            this.groupBox3.Location = new System.Drawing.Point(670, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(214, 611);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "已创建连接";
            // 
            // textBox_ThreadNum
            // 
            this.textBox_ThreadNum.Location = new System.Drawing.Point(352, 35);
            this.textBox_ThreadNum.Name = "textBox_ThreadNum";
            this.textBox_ThreadNum.Size = new System.Drawing.Size(100, 21);
            this.textBox_ThreadNum.TabIndex = 14;
            this.textBox_ThreadNum.Text = "1";
            this.textBox_ThreadNum.TextChanged += new System.EventHandler(this.textBox_ThreadNum_TextChanged);
            this.textBox_ThreadNum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_ThreadNum_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 10F);
            this.label1.Location = new System.Drawing.Point(236, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "创建客户端个数:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.textBox_ThreadNum);
            this.groupBox4.Controls.Add(this.textBox_Address);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.button_Start);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(649, 74);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "创建连接";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1114, 623);
            this.tableLayoutPanel1.TabIndex = 16;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.groupBox4, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 6);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(655, 611);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.groupBox2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(893, 6);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 92F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(215, 611);
            this.tableLayoutPanel3.TabIndex = 13;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.button_ReturnSendMessge, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.button_ReturnSendObject, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.button_SendMessge, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.button_SenObject, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 522);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(209, 86);
            this.tableLayoutPanel4.TabIndex = 11;
            // 
            // button_ReturnSendMessge
            // 
            this.button_ReturnSendMessge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_ReturnSendMessge.Font = new System.Drawing.Font("宋体", 10F);
            this.button_ReturnSendMessge.Location = new System.Drawing.Point(3, 37);
            this.button_ReturnSendMessge.Name = "button_ReturnSendMessge";
            this.button_ReturnSendMessge.Size = new System.Drawing.Size(98, 46);
            this.button_ReturnSendMessge.TabIndex = 9;
            this.button_ReturnSendMessge.Text = "发送消息带返回";
            this.button_ReturnSendMessge.UseVisualStyleBackColor = true;
            this.button_ReturnSendMessge.Click += new System.EventHandler(this.button_ReturnSendMessge_Click);
            // 
            // button_ReturnSendObject
            // 
            this.button_ReturnSendObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_ReturnSendObject.Font = new System.Drawing.Font("宋体", 10F);
            this.button_ReturnSendObject.Location = new System.Drawing.Point(107, 37);
            this.button_ReturnSendObject.Name = "button_ReturnSendObject";
            this.button_ReturnSendObject.Size = new System.Drawing.Size(99, 46);
            this.button_ReturnSendObject.TabIndex = 10;
            this.button_ReturnSendObject.Text = "发送对象带返回";
            this.button_ReturnSendObject.UseVisualStyleBackColor = true;
            this.button_ReturnSendObject.Click += new System.EventHandler(this.button_ReturnSendObject_Click);
            // 
            // button_SendMessge
            // 
            this.button_SendMessge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_SendMessge.Font = new System.Drawing.Font("宋体", 10F);
            this.button_SendMessge.Location = new System.Drawing.Point(3, 3);
            this.button_SendMessge.Name = "button_SendMessge";
            this.button_SendMessge.Size = new System.Drawing.Size(98, 28);
            this.button_SendMessge.TabIndex = 7;
            this.button_SendMessge.Text = "发送消息";
            this.button_SendMessge.UseVisualStyleBackColor = true;
            this.button_SendMessge.Click += new System.EventHandler(this.button_SendMessge_Click);
            // 
            // button_SenObject
            // 
            this.button_SenObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_SenObject.Font = new System.Drawing.Font("宋体", 10F);
            this.button_SenObject.Location = new System.Drawing.Point(107, 3);
            this.button_SenObject.Name = "button_SenObject";
            this.button_SenObject.Size = new System.Drawing.Size(99, 28);
            this.button_SenObject.TabIndex = 8;
            this.button_SenObject.Text = "发送对象";
            this.button_SenObject.UseVisualStyleBackColor = true;
            this.button_SenObject.Click += new System.EventHandler(this.button_SenObject_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("宋体", 10F);
            this.button1.Location = new System.Drawing.Point(494, 44);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 26);
            this.button1.TabIndex = 15;
            this.button1.Text = "开始连接(端口333)";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Client_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1143, 647);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Client_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Client";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.TextBox textBox_Address;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox richTextBox_Messge;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox richTextBox_SendMessge;
        private System.Windows.Forms.ListBox listBox_ConnList;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox_ThreadNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button button_SendMessge;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button button_SenObject;
        private System.Windows.Forms.Button button_ReturnSendMessge;
        private System.Windows.Forms.Button button_ReturnSendObject;
        private System.Windows.Forms.Button button1;
    }
}

